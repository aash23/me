---
layout: post
title: Principles of Knowledge Engineering
description: Principles of Knowledge Engineering
date: 2021-10-28
---
> By Prof. Patrick Heny Winston

I came across these principles when learning the course - [Artificial Intelligence](https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-034-artificial-intelligence-fall-2010/index.htm). These principles were enumerated by the Professor when he was explaining about build Rule Based Expert Systems. The problem was to build a grocery packing machine. Here are the following 3 principles:
1. Ask about specific cases. Know the specifics in order to gather knowledge that the expert (the grocery packing expert in our case) would not have thought to give you.
2. Ask questions about the things that appear to be the same, but are actually handled differently
3. Build a system and see when it cracks

The above set of rules, as explained by the professor, are also the rules that we unknowingly apply while trying to learning anything new or trying to build something. We also are, in a very simplied form, an expert system trying to learn about things.
